package silence;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class XMLFileInterpreter {

  public static void main(String[] args) {

    if (args.length < 4) {
      System.out.println(" You should pass 4 arguments\n\t1-path, 2-silenceDuration\n\t3-maxSegmentDuration, 4-silenceDurationForSegment \n\n\tSample input: '/path/to/xml/file' 3 1200 1.5");
      return;
    }
    String xmlFilePath = args[0];
    Float silenceDurationChapter, maxDurationOfSegment, silenceDurationSegment;

    try {
      silenceDurationChapter = Float.valueOf(args[1]).floatValue();
      maxDurationOfSegment = Float.valueOf(args[2]).floatValue();
      silenceDurationSegment = Float.valueOf(args[3]).floatValue();
    } catch (NumberFormatException e) {
      e.printStackTrace();;
      return;
    }

    try {
      File xmlFile = new File(xmlFilePath);
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db;
      try {
        db = dbf.newDocumentBuilder();
        try {
          Document document = db.parse(xmlFile);
          JSONObject jsonObject = new JSONObject();
          JSONArray chapters = new JSONArray();
          JSONArray chapterParts = new JSONArray();
          Long from, until, currentOffset = (long) 0;
          Integer currentChapter = 1, currentPart = 0, chapterPart = 0;

          document.getDocumentElement().normalize();
          System.out.println("Root element(tag): <" + document.getDocumentElement().getNodeName() + ">");
          NodeList nodeList = document.getElementsByTagName("silence");

          jsonObject.put("offset", "PT0S");
          jsonObject.put("tittle", "Chapter 1, Part 1");
          chapters.add(jsonObject);

          for (int itr = 0; itr < nodeList.getLength(); itr++) {
            Node node = nodeList.item(itr);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
              Element eElement = (Element) node;
              from = java.time.Duration.parse(eElement.getAttribute("from")).getSeconds();
              until = java.time.Duration.parse(eElement.getAttribute("until")).getSeconds();

              if (until - from >= silenceDurationSegment) {
                currentPart += 1;

                if (until - from >= silenceDurationChapter) {
                  currentPart = 1;
                  currentChapter += 1;
                  chapterPart = 0;
                  chapterParts.clear(); // new chapter
                }

                if (until - currentOffset > maxDurationOfSegment) {
                  currentPart = 1;
                  chapters.remove(chapters.size() - 1);
                  chapters.addAll(chapterParts);
                  currentPart += 1;
                } else {
                  jsonObject = new JSONObject();
                  jsonObject.put("title", "Chapter " + currentChapter + ", Part " + currentPart);
                  jsonObject.put("offset", java.time.Duration.parse(eElement.getAttribute("until")).toString());
                  chapters.add(jsonObject);
                  currentOffset = until;
                }
              }

              if (until - from >= 1) {
                chapterPart += 1;
                jsonObject = new JSONObject();
                jsonObject.put("title", "Chapter " + currentChapter + ", Part " + chapterPart);
                jsonObject.put("offset", java.time.Duration.parse(eElement.getAttribute("until")).toString());
                chapterParts.add(jsonObject);
              }
            }
          }
          ObjectMapper objectMapper = new ObjectMapper();
          String formattedJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectMapper.readTree(chapters.toJSONString()));
          System.out.println(formattedJson);
        } catch (SAXException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      } catch (ParserConfigurationException e) {
        e.printStackTrace();
      }
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
      return;
    } catch (NullPointerException e) {
      e.printStackTrace();
      return;
    } catch (Exception e) {
      e.printStackTrace();
      System.out.println("Something went wrong... :(");
    }

  }
}
